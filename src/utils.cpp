#include "IDReaper/Utils.h"
#include <cstring>

namespace reaper
{
	int CutLine(char* line, int wordsPositions[], int maxWords, char separators[])
	{
		int strLen = strlen(line);
		int wordsCount = 0;
		bool wordStart = false;
		bool commentStart = false;

		for (int i = 0; i < strLen && wordsCount < maxWords; ++i)
		{
			if (!strchr(separators, line[i]))
			{
				if (line[i] == '/' && i + 1 < strLen && line[i + 1] == '/')
				{
					// just comment
					break;
				}
				else if (line[i] == '/' && i + 1 < strLen && line[i + 1] == '*')
				{
					commentStart = true;
				}
				else if (line[i] == '*' && i + 1 < strLen && line[i + 1] == '/')
				{
					commentStart = false;
				}
				else if (!wordStart && !commentStart)
				{
					wordStart = true;
					wordsPositions[wordsCount++] = i;
				}
			}
			else
			{
				if (wordStart)
				{
					wordStart = false;
					line[i] = '\0';
				}
			}
		}

		return wordsCount;
	}

}//namespace reaper
