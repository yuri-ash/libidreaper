#include "IDReaper/reaper.h"
#include "IDReaper/utils.h"

namespace reaper
{
	Reaper::Reaper()
	{}

	Reaper::~Reaper()
	{
		std::list<char*>::iterator currentFileName = m_filesDone.begin();
		for (; currentFileName != m_filesDone.end(); ++currentFileName)
		{
			delete *currentFileName;
		}
		m_filesDone.clear();
	}

	int Reaper::QueryExternIDs(const char* filePath)
	{
		//errors accumulator
		int errs = 0;
		const char* pathEnd = strrchr(filePath, '\\');
		if (!pathEnd)
		{
			pathEnd = strrchr(filePath, '/');
		}
		if (!pathEnd)
		{
			strcpy(m_currentFolder, "./");
			errs += QueryExternIDsInFile(filePath);
		}
		else
		{
			strncpy(m_currentFolder, filePath, pathEnd - filePath);
			m_currentFolder[pathEnd - filePath] = '\0';
			int directoryLen = strlen(m_currentFolder);
			if (m_currentFolder[directoryLen] != '\\' && m_currentFolder[directoryLen] != '/')
			{
				strcat(m_currentFolder, "/");
			}
			char fileName[64] = { 0 };
			strcpy(fileName, filePath + (int)(pathEnd - filePath) + 1);
			errs += QueryExternIDsInFile(fileName);
		}

		return errs;
	}


	int Reaper::QueryExternIDsInFile(const char* fileName)
	{
		//errors accumulator
		int errs = 0;
		std::list<char*>::iterator currentFileName = m_filesDone.begin();
		while (currentFileName != m_filesDone.end() && strcmp(*currentFileName, fileName))
		{
			++currentFileName;
		}

		if (currentFileName == m_filesDone.end())
		{
			char* newFileValue = new char[strlen(fileName) + 1];
			strcpy(newFileValue, fileName);
			m_filesDone.push_back(newFileValue);
			char fullPath[512] = { 0 };
			strcpy(fullPath, m_currentFolder);
			strcat(fullPath, fileName);

			std::ifstream currentFile(fullPath);

			const char* extension = strrchr(fileName, '.');

			if (currentFile.is_open())
			{
				if (extension && !strcasecmp(extension, ".cs"))
				{
					errs += SearchForIDsCS(&currentFile);
				}
				else
				{
					errs += SearchForIDs(&currentFile);
				}
			}
			else
			{
				//std::cerr << "DataBase ERROR > Failed to open extern ID file \"" << fullPath << "\"!\n";
			}

			currentFile.close();
		}
		return errs;
	}

	int Reaper::SearchForIDsCS(std::ifstream* currentFile)
	{
		//errors accumulator
		int errs = 0;
		bool skipNextLine = false;
		int lineCount = 0;
		while (!currentFile->eof())
		{
			char line[2048] = { 0 };
			currentFile->getline(line, 2048);
			++lineCount;
			if (!skipNextLine)
			{
				int wordsPosition[16];
				int wordsCount = CutLine(line, wordsPosition, 16);

				if (wordsCount > 0)
				{
					if (!strcmp(&line[wordsPosition[0]], "enum") || wordsCount > 1 && !strcmp(&line[wordsPosition[1]], "enum"))
					{
						errs += ParseEnum(currentFile);
					}
					else
					{
						for (int i = 0; i < wordsCount; ++i)
						{
							if (!strcmp(&line[wordsPosition[i]], "="))
							{
								errs += AddExternIDs(&line[wordsPosition[i - 1]], atoi(&line[wordsPosition[i + 1]]));
							}
						}
					}
				}
			}
			else
			{
				skipNextLine = false;
			}
		}
		return errs;
	}

	int Reaper::SearchForIDs(std::ifstream* currentFile)
	{
		//errors accumulator
		int errs = 0;

		bool skipNextLine = false;
		int lineCount = 0;
		while (!currentFile->eof())
		{
			char line[2048] = { 0 };
			currentFile->getline(line, 2048);
			++lineCount;
			if (!skipNextLine)
			{
				int wordsPosition[16];
				int wordsCount = CutLine(line, wordsPosition, 16);

				if (wordsCount > 0)
				{
					if (!strcmp(&line[wordsPosition[0]], "#define"))
					{
						if (wordsCount == 3)
						{
							errs += AddExternIDs(&line[wordsPosition[1]], atoi(&line[wordsPosition[2]]));
						}
					}
					else if (!strcmp(&line[wordsPosition[0]], "#include"))
					{
						errs += QueryExternIDsInFile(&line[wordsPosition[1]]);
					}
					else if (!strcmp(&line[wordsPosition[0]], "#ifndef"))
					{
						skipNextLine = true;
					}
					else if ((!strcmp(&line[wordsPosition[0]], "typedef") && wordsCount > 1 && !strcmp(&line[wordsPosition[1]], "enum"))
						|| (!strcmp(&line[wordsPosition[0]], "enum"))
						)
					{
						errs += ParseEnum(currentFile);
					}
					else if (!strcmp(&line[wordsPosition[0]], "final") && !strcmp(&line[wordsPosition[1]], "static"))
					{
						if (!strcmp(&line[wordsPosition[2]], "int")
							|| !strcmp(&line[wordsPosition[2]], "short")
							|| !strcmp(&line[wordsPosition[2]], "byte"))
						{
							if (!strcmp(&line[wordsPosition[4]], "="))
							{
								errs += AddExternIDs(&line[wordsPosition[3]], atoi(&line[wordsPosition[5]]));
							}
						}
					}
					else if (!strcmp(&line[wordsPosition[1]], "final") && !strcmp(&line[wordsPosition[2]], "static"))
					{
						if (!strcmp(&line[wordsPosition[3]], "int")
							|| !strcmp(&line[wordsPosition[3]], "short")
							|| !strcmp(&line[wordsPosition[3]], "byte"))
						{
							if (!strcmp(&line[wordsPosition[5]], "="))
							{
								errs += AddExternIDs(&line[wordsPosition[4]], atoi(&line[wordsPosition[6]]));
							}
						}
					}
					else if (wordsCount == 3)
					{
						if (!strcmp(&line[wordsPosition[1]], "="))
						{
							errs += AddExternIDs(&line[wordsPosition[0]], atoi(&line[wordsPosition[2]]));
						}
					}
				}
			}
			else
			{
				skipNextLine = false;
			}
		}
		return errs;
	}

	int Reaper::AddExternIDs(const char* stringID, const int intID)
	{
		std::map<std::string, int, string_less>::iterator it = m_externIDs.find(stringID);

		if (it != m_externIDs.end())
		{
			std::cout << "DataBase WARNING > Duplicate extern ID found! \"" << stringID << "\" is already referenced.\n";
			return 0;//1 error happened
		}

		m_externIDs.insert(std::make_pair(std::string(stringID), intID));


		//std::list<ExternId>::iterator currentID = m_externIDs.begin();
		//for(; currentID != m_externIDs.end(); ++currentID)
		//{
		//	if (!strcasecmp((*currentID).m_stringID, stringID))
		//	{
		//		std::cerr << "DataBase ERROR > Duplicate extern ID found! \"" << stringID << "\" is already referenced.\n";
		//		return;
		//	}
		//}

		//ExternId newID;
		//strcpy(newID.m_stringID, stringID);
		//newID.m_intID = intID;
		//m_externIDs.push_back(newID);

		return 0;// no error
	}

	int Reaper::ParseEnum(std::ifstream* currentFile)
	{
		//error accumulator
		int errs = 0;
		bool inEnum = true;
		int currentEnumValue = 0;
		while (!currentFile->eof() && inEnum)
		{
			char line[1024] = { 0 };
			currentFile->getline(line, 1024);
			int wordsPosition[16];
			int wordsCount = CutLine(line, wordsPosition, 16, "\n \t,");

			if (wordsCount > 0)
			{
				if (!strcmp(&line[wordsPosition[0]], "{")
					|| line[wordsPosition[0]] == '/')
				{
					continue;
				}
				else if (strchr(&line[wordsPosition[0]], '}'))
				{
					inEnum = false;
				}
				else
				{
					if (wordsCount > 4)
					{
						currentEnumValue = atoi(&line[wordsPosition[2]]);
						if (!strcmp(&line[wordsPosition[3]], "<<"))
						{
							currentEnumValue <<= atoi(&line[wordsPosition[4]]);
						}
						else if (!strcmp(&line[wordsPosition[3]], ">>"))
						{
							currentEnumValue >>= atoi(&line[wordsPosition[4]]);
						}
					}
					else if (wordsCount > 2)
					{
						currentEnumValue = atoi(&line[wordsPosition[2]]);
					}
					errs += AddExternIDs(&line[wordsPosition[0]], currentEnumValue);
					++currentEnumValue;
				}
			}
		}
		return errs;
	}

	int Reaper::ResolveExternIDs(const char* externID)
	{
		if (externID)
		{
			std::map<std::string, int, string_less>::iterator it = m_externIDs.find(externID);
			if (it != m_externIDs.end())
				return it->second;

			//std::list<ExternId>::iterator currentID = m_externIDs.begin();
			//for(; currentID != m_externIDs.end(); ++currentID)
			//{
			//	if (!strcasecmp((*currentID).m_stringID, externID))
			//	{
			//		return (*currentID).m_intID;
			//	}
			//}

			if (!strcasecmp(externID, "0") || !strcasecmp(externID, "NULL"))
			{
				return 0;
			}
		}

		std::cerr << "DataBase ERROR > Can't find extern ID \"" << externID << "\"!\n";
		return -1;
	}
}//namespace reaper