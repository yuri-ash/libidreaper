#ifndef __UTILS_H__
#define __UTILS_H__

#include <string>
namespace reaper
{
	int CutLine(char* line, int wordsPositions[], int maxWords, char separators[] = "\n\t \"");
}//namespace reaper

#endif //__UTILS_H__
