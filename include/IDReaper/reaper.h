#ifndef __IDREAPER_H__
#define __IDREAPER_H__

#include <iostream>
#include <fstream>
#include <map>
#include <list>

#if defined _WINDOWS
#define strcasecmp(str1, str2)	stricmp(str1, str2)
#endif //_WINDOWS

#ifdef MAX_PATH_LEN
#	undef MAX_PATH_LEN
#endif //MAX_PATH_LEN
#define MAX_PATH_LEN 2048

struct string_less
{
	bool operator() (const std::string& _Left, const std::string& _Right) const
	{
		return strcasecmp(_Left.c_str(), _Right.c_str()) < 0 ? true : false;
	}
};

namespace reaper
{
	class Reaper
	{
	public:
		/**
		* Default constructor 
		*/
		Reaper();

		/**
		* Destructor
		*/
		virtual ~Reaper();

		int QueryExternIDs(const char* filePath);
		int ResolveExternIDs(const char* externID);

	protected:
		int QueryExternIDsInFile(const char* fileName);
		int AddExternIDs(const char* stringID, const int intID);
		int SearchForIDs(std::ifstream* currentFile);
		int SearchForIDsCS(std::ifstream* currentFile);
		int ParseEnum(std::ifstream* currentFile);

	private:
		std::map<std::string, int, string_less>		m_externIDs;
		std::list<char*>	m_filesDone;
		char				m_currentFolder[MAX_PATH_LEN];
	};

}//namespace reaper

#endif //__IDREAPER_H__